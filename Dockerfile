FROM registry.gitlab.com/andrewheberle/docker-base:alpine3.15

ARG DUPLICACY_WEB_VERSION=1.5.0
ARG DUPLICACY_ARCH=x64

RUN apk --no-cache add ca-certificates tzdata dbus && \
    mkdir -p /config /logs /cache && \
    wget -q -O /usr/bin/duplicacy_web "https://acrosync.com/duplicacy-web/duplicacy_web_linux_${DUPLICACY_ARCH}_${DUPLICACY_WEB_VERSION}" && \
    chmod +x /usr/bin/duplicacy_web && \
    addgroup -g 10001 duplicacy && \
    adduser -h /home/duplicacy -s /sbin/nologin -D -u 10001 -G duplicacy -g "Duplicacy Web" duplicacy && \
    rm -f /var/lib/dbus/machine-id /etc/machine-id && \
    ln -s /config/machine-id /var/lib/dbus/machine-id && \
    ln -s /config/machine-id /etc/machine-id && \
    ln -s /config/ /home/duplicacy/.duplicacy-web

COPY rootfs /

ENV DWE_PASSWORD="supersecretpassword"

VOLUME [ "/config", "/logs", "/cache" ]
