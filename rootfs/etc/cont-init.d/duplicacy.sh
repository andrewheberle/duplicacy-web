#!/bin/sh
set -e

# Preparing persistent unique machine ID
if ! dbus-uuidgen --ensure=/config/machine-id; then 
    dbus-uuidgen > /config/machine-id
fi

# Set required default settings
if [ ! -f /config/settings.json ]; then
    echo '{
        "listening_address"     : "0.0.0.0:3875",
        "log_directory"         : "/logs",
        "temporary_directory"   : "/cache"
    }' > /config/settings.json
fi

if [ ! -f /config/duplicacy.json ]; then
    echo '{}' > /config/duplicacy.json
fi

chown -R duplicacy:duplicacy /config /logs /cache
