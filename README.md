# duplicacy-web

This is an attempt to run Duplicacy Web as a container under Kubernetes.

## Why

Existing containers didn't seem a good fit.

## Usage

The following will get up and running however there are some customisations that should be done before this:

```sh
kubectl create namespace duplicacy-web
kubectl create -f manifests/*.yml
```

### Ingress

Changes to the ingress will be required such as the `host` and if you require SSL/TLS termination the addition of certifcate details.

```yaml
spec:
  rules:
    host: <change-this-to-match-your-environment>
```

### Network Policy

The configured network policy assumes you are using the HAProxy Kubernetes Ingress Controller so it only allows ingress traffic from those pods to the duplicacy-web pod.

Update the `podSelector` to match if you are using an alternative ingress controller:

```yaml
spec:
  ingress:
  - from:
    - podSelector:
        matchLabels:
          app.kubernetes.io/instance: haproxy
          app.kubernetes.io/name: haproxy
```